import { memo, useCallback, useState } from 'react';

// use State + DRILLING PROPS

export default function Demo1useState () {
  const [value1, setValue1] = useState(0);
  const [value2, setValue2] = useState(0);

  console.log('Container')

  const inc = useCallback(() => {
    setValue1(s => s + 1);
    setValue2(s => s + 1);
  }, [])

  return (
    <div>
      <h3>Container</h3>
      <button onClick={() => setValue1(prev => prev + 1)}>{value1}</button>
      <button onClick={() => setValue2(prev => prev + 1)}>{value2}</button>
      <Parent data1={value1} data2={value2} increment={inc}/>

    </div>
  );
}

// ========================
interface ParentProps {
  data1: number;
  data2: number;
  increment: () => void;
}
function Parent(props: ParentProps) {
  console.log(' parent')
  return <div>
    <ChildA value={props.data1} />
    <ChildB value={props.data2}/>
    <ChildC increment={props.increment}/>
  </div>
}

// ========================
interface ChildAProps { value: number }

const ChildA = memo((props: ChildAProps) => {
  console.log('  child A')
  return <div>
    Child A:  {props.value}
  </div>
})

// ========================
interface ChildBProps { value: number }

// MEMOIZZAZIONE
export const ChildB = memo((props: ChildBProps) => {
  console.log('  child B')
  return <div>
    Child B: {props.value}
  </div>
})
// ChildB.displayName = 'ChildB'

// ========================
interface ChildCProps {
  increment: () => void;
}
const ChildC = memo((props: ChildCProps) => {
  console.log('  Child C')
  return <div>
    Child C
    <button onClick={props.increment}>+</button>
  </div>
})
