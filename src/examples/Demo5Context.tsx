// Static Template
import { createContext, memo, useContext, useState } from 'react';

interface Data {
  value: number;
}

interface Actions {
  increment: () => void;
}

export const Data1Context = createContext<Data>({ value: -1 })
export const Data2Context = createContext<Data>({ value: -1 })
export const ActionsContext = createContext<Actions>( { increment: () => null })


export const Demo5Context = () => {
  console.log('Container')
  const [value1, setValue1] = useState<Data>({ value: 0});
  const [value2, setValue2] = useState<Data>({ value: 0});

  function inc() {
    setValue1(s => ({...s, value: s.value + 1}))
  }

  return (
    <div>
      <ActionsContext.Provider value={{ increment: inc }}>
        <Data1Context.Provider value={ value1 }>
          <Data2Context.Provider value={ value2 }>
            <button onClick={inc}>Update Value 1</button>
            <button onClick={() => setValue2({ value: Math.random() })}>Update Value 2</button>
            <Parent />
          </Data2Context.Provider>
        </Data1Context.Provider>
      </ActionsContext.Provider>
    </div>
  );
}



const Parent = () => {
  console.log(' Parent')
  return <div>
    Parent
    <Child1 />
    <Child2 />
    <Child3 />
  </div>
}



const Child1 = memo(() => {
  const state = useContext(Data1Context)
  console.log('  Child1')
  return <div>Child #1: {state.value}</div>
})


const Child2 = memo(() => {
  const state = useContext(Data2Context)
  console.log('  Child2')
  return <div>Child #2: {state.value}</div>
})


const Child3 = memo(() => {
  const state = useContext(ActionsContext)
  console.log('  Child3')
  return <div>Child #3 <button onClick={state.increment}>+</button></div>
})
