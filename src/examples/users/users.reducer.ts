import { User } from '../../model/user.ts';
import { UsersActions } from './users.actions.ts';

interface UsersState {
  list: Partial<User>[];
  pending: boolean;
  error: boolean;
}

export const initialState: UsersState = { list: [], pending: false, error: false }

export function usersReducer(state: UsersState, action: UsersActions) {
  switch (action.type) {
    case 'load':
      return {...state, list: action.payload}
    case 'deleteUser':
      return {...state, list: state.list.filter(user => user.id !== action.payload)}
    case 'addUser':
      return ({...state, list: [...state.list, action.payload ]});
  }
  return state;
}

