import React, { useEffect, useReducer } from 'react';
import { User } from '../../model/user.ts';
import { addUserAPI, deleteUserAPI, getUsersAPI } from './users.api.ts';
import { initialState, usersReducer } from './users.reducer.ts';

export function useUsers() {
  const [state, dispatch] = useReducer(usersReducer, initialState)

  useEffect(() => {
    getUsersAPI()
      .then(res => {
        dispatch({ type: 'load', payload: res.data})
      })
  }, [])

  function deleteUser(id: number) {
    deleteUserAPI(id)
      .then(() => {
        dispatch({ type: 'deleteUser', payload: id })
      })
  }

  function addUser(e: React.KeyboardEvent<HTMLInputElement>) {
    const target = e.currentTarget;

    if(e.key === 'Enter') {
      const user: Pick<User, 'name'> = { name: target.value }
      addUserAPI(user)
        .then(res => {
          dispatch({ type: 'addUser', payload: res.data})
          target.value = ''
        })
    }
  }


  return {
    addUser,
    deleteUser,
    state
  }
}
