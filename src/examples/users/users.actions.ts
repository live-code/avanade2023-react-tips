import { User } from '../../model/user.ts';

interface Load { type: 'load', payload: User[] }
interface DeleteUser { type: 'deleteUser', payload: number }
interface AddUser { type: 'addUser', payload: Partial<User> }

export type UsersActions = Load | DeleteUser | AddUser;
