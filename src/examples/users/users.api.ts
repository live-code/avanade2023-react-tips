import axios from 'axios';
import { User } from '../../model/user.ts';

export function getUsersAPI() {
  return axios.get<User[]>('https://jsonplaceholder.typicode.com/users')
}
export function deleteUserAPI(id: number) {
  return axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
}
export function addUserAPI(user: Pick<User, 'name'>) {
  return axios.post<Pick<User, 'id' | 'name'>>('https://jsonplaceholder.typicode.com/users',  user)
}
