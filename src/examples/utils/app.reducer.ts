
// useReducer + COMPOSITION
interface AppState {
  value1: number;
  value2: number;
  subscribe: boolean;
}

export const initialState: AppState =  {
  value1: 0, value2: 0, subscribe: false
};

export function appReducer(state: AppState, action: AppActions) {
  switch (action.type) {
    case 'incrementAll':
      return {...state, value1: state.value1 + 10, value2: state.value2 + 10}

    case 'incrementValue1':
      return { ...state, value1: state.value1 + 1}
    case 'incrementValue2':
      return { ...state, value2: state.value2 + action.payload}
    case 'subscribe':
      return { ...state, subscribe: action.payload}
  }
  return state;
}
