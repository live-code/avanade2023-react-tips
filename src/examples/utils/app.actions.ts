

interface IncrementValue1 {
  type: 'incrementValue1'
}

interface IncrementValue2 {
  type: 'incrementValue2',
  payload: number;
}
interface IncrementAll {
  type: 'incrementAll',
}

interface Subscribe {
  type: 'subscribe';
  payload: boolean;
}

type AppActions =
  IncrementValue1 |
  IncrementValue2 |
  Subscribe |
  IncrementAll
