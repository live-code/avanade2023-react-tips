import { useUsers } from './users/useUsers.ts';

export default function() {

  const {addUser, deleteUser, state } = useUsers();

  return <div>
    CRUD use Reducer

    <input type="text" onKeyDown={addUser} />

    {
      state.list.map(item => {
        return <li key={item.id}>
          {item.name}
          <button onClick={() => deleteUser(item.id!)}>Delete</button>
        </li>
      })
    }

  </div>
}

