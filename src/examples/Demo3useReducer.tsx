import { memo, PropsWithChildren, useReducer } from 'react';
import { initialState, appReducer } from './utils/app.reducer.ts';


export default function Demo3useReducer () {
  const [state, dispatch] = useReducer(appReducer, initialState)

  function inc1() {
    dispatch({ type: 'incrementValue1' })
  }

  return (
    <div>
      <h3>Container {state.value1} - {state.value2}</h3>

      <button onClick={inc1}>{state.value1}</button>
      <button onClick={() => dispatch({ type: 'incrementValue2', payload: 2 })}>{state.value2}</button>
      <button onClick={() => dispatch({ type: 'subscribe', payload: true })}>{JSON.stringify(state.subscribe)}</button>
      <button onClick={() => dispatch({ type: 'incrementAll' })}>Inc All</button>

      <Parent>
        <ChildA value={state.value1} />
        <ChildB value={state.value2} />
        <ChildC dispatch={dispatch} />
      </Parent>
    </div>
  );
}


// ========================
function Parent(props: PropsWithChildren) {
  return <div>
      <h1>PARENT</h1>
      <hr/>
      {props.children}
  </div>
}

// ========================
interface ChildAProps { value: number }

const ChildA = memo((props: ChildAProps) => {
  console.log('  child A')
  return <div>
    Child A:  {props.value}
  </div>
})

// ========================
interface ChildBProps { value: number }

// MEMOIZZAZIONE
export const ChildB = memo((props: ChildBProps) => {
  console.log('  child B')
  return <div>
    Child B: {props.value}
  </div>
})
// ChildB.displayName = 'ChildB'

// ========================
interface ChildCProps {
  dispatch:  any;
}
const ChildC = (props: ChildCProps) => {
  console.log('  Child C')
  return <div>
    Child C
    <button onClick={() => props.dispatch({ type: 'incrementValue1' })}>+</button>
  </div>
}
