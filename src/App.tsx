import { Demo5Context } from './examples/Demo5Context.tsx';

function App() {

  return (
    <>
    {/*  <Demo1useState />*/}
      {/*<Demo2useStateComposition />*/}
     {/* <Demo3useReducer />*/}
      {/*<Demo4useReducerCrud />*/}
      <Demo5Context />
    </>
  )
}

export default App
